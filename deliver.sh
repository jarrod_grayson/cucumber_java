logs=$(base64 -w 0 target/*.json)

echo -n '{ "testcycle" : 2615021, "result" : "' > payload.json
echo -n $logs >> payload.json
echo -n '", "projectId" : 87143 }' >> payload.json

curl -X POST \
 https://pulse-7.qtestnet.com/webhook/0fd02d1e-47fc-4c3d-83f4-740ed24f2005 \
 -H 'cache-control: no-cache' \
 -H 'content-type: application/json' \
 -d @payload.json